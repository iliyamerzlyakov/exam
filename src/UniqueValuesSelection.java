import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Отбор униклаьных значений
 * @author Merzlyakov I.
 */
public class UniqueValuesSelection {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        System.out.println("Все значения: " + arrayFilling(list));
        System.out.println("Уникальные значения: " + uniqueValuesSelection(list));
    }

    /**
     * Заполняет массив числами
     * @param list пустой массив
     * @return заполненный массив
     */
    private static ArrayList arrayFilling(ArrayList list) {
        list.add(-3);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(5);
        list.add(6);
        return list;
    }

    /**
     * Отбирает уникальные значения
     * @param list массив чисел
     * @return массив уникальных значений
     */
    private static ArrayList uniqueValuesSelection(ArrayList list) {
        Set uniqueValues = new HashSet(list);
        return new ArrayList(uniqueValues);
    }
}